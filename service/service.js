var BASE_URL = "https://636150a5af66cc87dc28e460.mockapi.io/";

export var getProductList = () => {
  return axios({
    url: `${BASE_URL}/Products`,
    method: "GET",
  });
};
