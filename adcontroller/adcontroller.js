import { Product } from "../model/model.js";

export function renderList(list) {
    let contentHTML = ``;
    list.forEach(function (item) {
        let contentTr = `<tr>
          <td>${item.id}</td>
          <td>${item.name}</td>
          <td>${item.price}</td>
          <td>${item.img}</td>
          <td>${item.desc}</td>
          <td>
              <button onclick="removeProduct(${item.id
            })" class="btn btn-danger mr-1">Xóa</button>
              <button onclick="watchProduct(${item.id
            })" class="btn btn-info">Xem</button>
          </td>
      </tr>`;
        contentHTML += contentTr;
    });
    document.getElementById(`tblDanhSachSP`).innerHTML = contentHTML;
}

export function layThongTinTuForm() {
    let name = document.getElementById(`TenSP`).value;
    let price = document.getElementById(`GiaSP`).value;
    let img = document.getElementById(`HinhSP`).value;
    let desc = document.getElementById(`MoTa`).value;
    return {
        name: name,
        price: price,
        img: img,
        desc: desc,
    };
}