
import { getProductList } from "../service/service.js"
import { Products } from "../model/model.js";
import { 
  renderList, 
  layThongTinTuForm 
} from "./adcontroller.js";


let idEdited = null;
// render
function fetchAllProducts() {
    axios({
        url: `${BASE_URL}/Products`,
        method: "GET",  
    })
    .then(function (res) {
        renderList(res.data);
    })
    .catch(function(err) {
        console.log('err: ', err);
    })
}
// chạy lần đầu khi load trang
fetchAllProducts();

// remove
function removeProducts(id) {
    console.log(`xóa`);
    axios({
        url: `${BASE_URL}/Products/${id}`,
        method: "DELETE",
    })
    .then(function (res) {
        fetchAllProducts();
    })
    .catch(function(err) {
        console.log('err: ', err);
    })
}

// Add 
function addProducts() {
    var data = layThongTinTuForm();

    let newProducts = {
        id: data.id,
        name: data.name,
        price: data.price,
        img: data.img,
        desc: data.desc,
    };
    axios({
        url: `${BASE_URL}/Products`,
        method: "POST",
        data: newProducts,
      })
        .then(function (res) {
          fetchAllProducts();
        })
        .catch(function (err) {
          console.log(`err: `, err);
        });
}

window.addProducts = addProducts;

// edit
function editProducts(id) {
    axios({
        url: `${BASE_URL}/Products/${id}`,
        method: "GET",
      })
        .then(function (res) {
          document.getElementById(`name`).value = res.data.name;
          document.getElementById(`price`).value = res.data.price;
          document.getElementById(`img`).value = res.data.img;
          document.getElementById(`desc`).value = res.data.desc;
          idEdited = res.data.id;
        })
        .catch(function (err) {
          console.log(`err: `, err);
        });
}

// update
function updateProducts() {
    var data = layThongTinTuForm();
    var newProducts = {
        id: data.id,
        name: data.name,
        price: data.price,
        img: data.img,
        desc: data.desc,
    }
    axios({
        url: `${BASE_URL}/Products/${idEdited}`,
        method: "PUT",
      })
        .then(function (res) {
          document.getElementById(`name`).value = res.data.name;
          document.getElementById(`price`).value = res.data.price;
          document.getElementById(`img`).value = res.data.img;
          document.getElementById(`desc`).value = res.data.desc;
          idEdited = res.data.id;
        })
        .catch(function (err) {
          console.log(`err: `, err);
        });
}