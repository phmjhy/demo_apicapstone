import { CartItem, Product } from "../model/model.js";
import { getProductList } from "../service/service.js";

export function createProductList(data) {
  let productList = [];
  data.forEach((item) => {
    let { id, name, price, screen, backCamera, frontCamera, img, desc, type } =
      item;
    let productItem = new Product(
      id,
      name,
      price,
      screen,
      backCamera,
      frontCamera,
      img,
      desc,
      type
    );
    productList.push(productItem);
  });
  return productList;
}

export function renderProductList(list) {
  let contentHTML = "";
  list.forEach((item, index) => {
    let { id, name, price, screen, backCamera, frontCamera, img, desc, type } =
      item;
    contentHTML += `
    <div class="card col-sm-2" style="width: 18rem">
        <img class="card-img-top" 
            src="${img}" 
            alt="Card image" />
        <div class="card-body">
            <h3 class="text-warning">${type}</h6>
            <h5 class="card-title">${name}</h5>
            <p class="card-text">screen: ${screen}</p>
            <p class="card-text">frontCamera: ${frontCamera}</p>
            <p class="card-text">backCamera: ${backCamera}</p>
            <p class="card-text">desc: ${desc}</p>
            <div class="d-flex justify-content-between">
                <p class="card-text text-danger font-weight-bold">${price}đ</p>
                <button class="btn btn-primary"
                    onclick="addToCart(${id})">Add to Cart</button>
            </div>
        </div>
    </div>
    `;
  });
  document.getElementById("product-list").innerHTML = contentHTML;
}

export function filterProduct(list, keyword) {
  let filteredList = [];
  list.forEach((item) => {
    if (item.type == keyword) {
      filteredList.push(item);
    }
  });
  return filteredList;
}

export function renderCart(cart) {
  let contentBody = "";
  let grandTotal = 0;
  cart.forEach((item, index) => {
    contentBody += `
        <tr>
            <td style="width: 20%;">
                <img style="max-height:100px;" 
                    src="${item.product.img}" 
                    alt="CartItem image" />
            </td>
            <td style="width: 20%;">${item.product.name}</td>
            <td style="width: 20%;">
                <div class="number-input d-flex align-items-center">
                    <button class="mx-3"
                     onclick="reduceItemQty(${item.product.id})">
                    <i class="fa fa-minus"></i></button>
                    <p>${item.qty}</p>
                    <button class="mx-3"
                     onclick="increaseItemQty(${item.product.id})">
                    <i class="fa fa-plus"></i></button>
                </div>
            </td>
            <td style="width: 20%;">${item.subtotal()}đ</td>
            <td style="width: 20%;">
                <button class="btn btn-danger" onclick="removeItem(${index})">Xóa</button>
            </td>
        </tr>
    `;
    grandTotal += item.subtotal();
  });

  let contentFooter = `
    <p class="font-weight-bold text-center text-primary display-4">Tổng tiền: ${grandTotal}đ</p>
    <button class="btn btn-success" onclick="clearCart()">Thanh toán</button>
    <button class="btn btn-danger" onclick="clearCart()">Xóa giỏ hàng</button>
  `;

  document.getElementById("cart-body").innerHTML = contentBody;
  document.getElementById("cart-footer").innerHTML = contentFooter;
}

