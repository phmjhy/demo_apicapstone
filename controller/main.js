import { CartItem, Product } from "../model/model.js";
import { getProductList } from "../service/service.js";
import {
  createProductList,
  renderProductList,
  filterProduct,
  renderCart,
} from "./controller.js";

var cart = [];
var currentProductList = [];

function saveCartLocal() {
  showCartQty();
  localStorage.setItem("myCart", JSON.stringify(cart));
}

function getCartLocal() {
  let dataJson = localStorage.getItem("myCart");
  if (dataJson !== null) {
    let tempArr = JSON.parse(dataJson);
    for (let i = 0; i < tempArr.length; i++) {
      let { product, qty } = tempArr[i];
      let cartItem = new CartItem(product, qty);
      cart.push(cartItem);
    }
  }
  showCartQty();
}

getCartLocal();

function showCartQty() {
  let cartQty = 0;
  cart.forEach((item) => {
    cartQty += item.qty;
  });
  document.getElementById("cart-qty").innerHTML = `(${cartQty} sp)`;
}

var fetchAllProducts = () => {
  getProductList()
    .then((res) => {
      currentProductList = createProductList(res.data);
      renderProductList(currentProductList);
    })
    .catch((err) => {
      console.log(err);
    });
};

fetchAllProducts();

var fetchFilteredProduct = () => {
  let filter = document.getElementById("filter-product");
  filter.addEventListener("change", function () {
    let keyword = document.getElementById("selectProduct").value;

    getProductList()
      .then((res) => {
        currentProductList = createProductList(res.data);
        if (keyword == "all") {
          renderProductList(currentProductList);
        } else {
          let list = filterProduct(currentProductList, keyword);
          renderProductList(list);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  });
};

fetchFilteredProduct();

function addToCart(selectedId) {
  let productItem = {};
  //find product object from selectedProductID
  currentProductList.forEach((item) => {
    if (item.id == selectedId) {
      productItem = item;
    }
  });

  //add cartItem to Cart
  let cartItem = new CartItem(productItem, 1);

  if (cart.length != 0) {
    let itemExist = false;
    cart.forEach((item) => {
      if (item.product.id == productItem.id) {
        item.qty++;
        itemExist = true;
      }
    });
    if (!itemExist) {
      cart.push(cartItem);
    }
  } else {
    cart.push(cartItem);
  }

  showCartQty();
  saveCartLocal();
}
window.addToCart = addToCart;

//view cart
document.getElementById("view-cart").addEventListener("click", function () {
  fetchCart();
  //hide product-body
  document.getElementById("product-body").style.display = "none";
});

//view product
document.getElementById("view-product").addEventListener("click", function () {
  document.getElementById("product-body").style.display = "block";
  document.getElementById("collapseCart").classList.remove("show");
});

function fetchCart() {
  if (cart.length != 0) {
    renderCart(cart);
  } else {
    document.getElementById(
      "cart-body"
    ).innerHTML = `<p class="text-danger">Bạn chưa có sản phẩm nào trong giỏ hàng!</p>`;
    document.getElementById("cart-footer").innerHTML = "";
  }
}

function reduceItemQty(productId) {
  cart.forEach((item, index) => {
    if (item.product.id == productId) {
      if (item.qty > 1) {
        item.qty--;
      } else {
        removeItem(index);
      }
    }
  });
  saveCartLocal();
  fetchCart();
}
window.reduceItemQty = reduceItemQty;

function increaseItemQty(productId) {
  cart.forEach((item) => {
    if (item.product.id == productId) {
      item.qty++;
    }
  });
  saveCartLocal();
  fetchCart();
}
window.increaseItemQty = increaseItemQty;

function removeItem(index) {
  cart.splice(index, 1);
  saveCartLocal();
  fetchCart();
}
window.removeItem = removeItem;

function clearCart() {
  cart = [];
  saveCartLocal();
  fetchCart();
}
window.clearCart = clearCart;
